package com.abbott.adc.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.abbott.adc.adapter.service.businessevent.client")
@EnableCircuitBreaker
public class AdcB2cWebshopAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdcB2cWebshopAdapterApplication.class, args);
	}
}
