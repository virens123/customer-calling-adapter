package com.abbott.adc.adapter.service.businessevent.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.abbott.adc.adapter.webshop.dto.Customer;

@FeignClient(name="webshop-customers-service", fallbackFactory = WebshopClientFallbackFactory.class)
public interface WebshopClient {

	@GetMapping("/api/customers/")
	public List<Customer> getCustomers();
	
	@GetMapping("/api/customers/{customerid}")
	public String getCustomersById(@PathVariable("customerid") Long customerid);

	/*@GetMapping("/api/accounts/")
	public List<Account> getAllAccount();
	
	@GetMapping("/api/accounts/{accountid}")
	public Account getAccountsById(@PathVariable("accountid") Long accountid);
	
	@PostMapping("/accounts")
	public Long createCustomer(@RequestBody Account acc);
	
	@PutMapping("/accounts")
	public void updateAccount(@RequestBody Long id);

	@GetMapping("/accounts/address/{customerid}")
	public ADC_Address__c getCustomerAddress(@PathVariable("customerid") Long customerid);

	@GetMapping("/customer/{customer_id}/address/{id}/retrieve")
	public ADC_Address__c getCustomerAddressByAddressId(@PathVariable("customerid") Long customer_id,@PathVariable("id") Long id);

	@PostMapping("/customers/{customer_id}/address/create")
	public Long createCustomerAddress(@RequestBody ADC_Address__c address__c,@PathVariable("customerid") Long customer_id);

	@PutMapping("/customers/{customer_id}/addresses/{address_id}/update")
	public void updateAddress(@PathVariable("customer_id") Long customer_id,@PathVariable("address_id") Long address_id);

	@GetMapping("/customers/communication")
	public List<ADC_Permission__c> getCommunicationDetails();

	@GetMapping("/customers/communication/{customerid}")
	public ADC_Permission__c getCommunicationDetailsById(@PathVariable("customerid") Long customerid);
*/}

