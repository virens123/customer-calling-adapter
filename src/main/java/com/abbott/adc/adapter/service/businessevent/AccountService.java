/*package com.abbott.adc.adapter.service.businessevent;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abbott.adc.adapter.service.businessevent.client.WebshopClient;
import com.abbott.adc.adapter.webshop.dto.Address;
import com.abbott.adc.adapter.webshop.dto.Communication;
import com.abbott.adc.adapter.webshop.dto.Customer;
import com.abbott.adc.adapter.webshop.mapper.AddressMapper;
import com.abbott.adc.adapter.webshop.mapper.CommunicationMapper;
import com.abbott.adc.adapter.webshop.mapper.CustomerMapper;
import com.sforce.soap.enterprise.sobject.ADC_Address__c;
import com.sforce.soap.enterprise.sobject.ADC_Permission__c;
import com.sforce.soap.enterprise.sobject.Account;

@Service
public class AccountService {

	@Autowired
	WebshopClient webshopClient;
	
	@Autowired
	CustomerMapper customerMapper;
	
	@Autowired
	CommunicationMapper communicationMapper;
	
	
	@Autowired
	AddressMapper addressMapper;

	public List<Customer> getCustomer() {
		return webshopClient.getCustomers();
	}
	
	public List<Account> getAllAccount() {
		return webshopClient.getAllAccount();
	}
	
	public Customer getCustomersById(Long customerid) throws Exception {
		return null;
	}
	
	*//**
	 * This method will fetch data for accountId and then will create the customer in magento.
	 * After creating customer data will update csr 
	 * @param accountId
	 * @return
	 * @throws Exception
	 *//*
	public Customer getAccountsById(Long accountId) throws Exception {
		Account account =  webshopClient.getAccountsById(accountId);
		if(null != account && account.getADC_Account_ID_18__c() != null) {
			Long id = webshopClient.createCustomer(account);
			updateAccount(id);
			
		}
		return customerMapper.csrToMagentoMapper(account);
	}
	
	*//**
	 * This method will update the CSR details after creating customer in Magento
	 * @param Id
	 *//*
	public void updateAccount(Long Id) {
		webshopClient.updateAccount(Id);
	}

	*//**
	 * This method is used to get customer Address from magento
	 * @param id
	 * @return
	 *//*
	public Address getCustomerAddress(Long id) {
		ADC_Address__c address__c =  webshopClient.getCustomerAddress(id);
		return addressMapper.cseToMagentoAddressMapper(address__c);
	}

	*//**
	 * This method will retrieve Customer address by customer id and address id
	 * @param customer_id
	 * @param id
	 * @return
	 *//*
	public Address getCustomerAddressByAddressId(Long customer_id, Long id) {
		ADC_Address__c address__c =  webshopClient.getCustomerAddressByAddressId(customer_id,id);
		if(null != address__c) {
			Long addressId = webshopClient.createCustomerAddress(address__c,customer_id);
			updateAddress(addressId,customer_id);
		}
		return addressMapper.cseToMagentoAddressMapper(address__c);
	}
	
	*//**
	 * This method will update the Address details in CSR after creating Address in Magento
	 * @param Id
	 *//*
	public void updateAddress(Long id,Long customer_id) {
		webshopClient.updateAddress(id,customer_id);
	}

	*//**
	 * Used to retrieve Communication details
	 * @return
	 *//*
	public List<ADC_Permission__c> getCommunicationDetails() {
		return webshopClient.getCommunicationDetails();
	}

	*//**
	 * Used to retrieve communication details by passing cust id
	 * @param id
	 * @return
	 *//*
	public Communication getCommunicationDetailsById(Long id) {
		ADC_Permission__c permission=  webshopClient.getCommunicationDetailsById(id);
		return communicationMapper.csrToMagentoCommunicationMapper(permission);
	}

	*//**
	 * Used to create account in csr
	 * @param acc
	 * @return
	 *//*
	public Long createCustomer(Account acc) {
		Long custid=  webshopClient.createCustomer(acc);
		return custid;
	}
}
*/