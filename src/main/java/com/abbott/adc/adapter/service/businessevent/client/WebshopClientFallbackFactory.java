package com.abbott.adc.adapter.service.businessevent.client;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;

@Component	
public class  WebshopClientFallbackFactory implements FallbackFactory<WebshopClient> {

	@Override
    public WebshopClient create(Throwable throwable) {
        return new WebshopClientFallback(throwable);
    }
	
	
}

