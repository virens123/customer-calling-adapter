package com.abbott.adc.adapter.service.businessevent;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abbott.adc.adapter.service.businessevent.client.WebshopClient;
import com.abbott.adc.adapter.webshop.dto.Customer;

@Service
public class CustomerService {
	
	@Autowired
	WebshopClient webshopClient;
	/*@Autowired
	CustomerMapper customerMapper;*/

	public List<Customer> getCustomer() {
		return webshopClient.getCustomers();
		
	}
	
	public String getCustomersById(Long customerid) throws Exception {
		System.out.println("**Service**");
		/*Account account =  webshopClient.getCustomersById(customerid);*/
		System.out.println("**End Service**");
		return webshopClient.getCustomersById(customerid);
		//return customerMapper.csrToMagentoMapper(account);
	}
	
}
