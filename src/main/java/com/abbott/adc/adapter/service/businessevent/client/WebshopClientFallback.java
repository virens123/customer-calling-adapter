package com.abbott.adc.adapter.service.businessevent.client;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.abbott.adc.adapter.webshop.dto.Customer;

import feign.FeignException;
public class  WebshopClientFallback implements WebshopClient {

	private final Throwable cause;

    public WebshopClientFallback(Throwable cause) {
      this.cause = cause;
    }
	
	@Override
	public List<Customer> getCustomers(){
		return new ArrayList<Customer>();
	}
	
	@Override
	public String getCustomersById(@PathVariable("customerid") Long customerid) {
		
		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
            System.out.println("404 enconter");
        }
		return "";
	}

	/*@Override
	public List<Account> getAllAccount() {
		return new ArrayList<Account>();
	}

	@Override
	public Account getAccountsById(Long accountid) {
		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
            System.out.println("404 enconter");
        }
		return new Account();
	}
	
	@Override
	public Long createCustomer(Account acc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateAccount(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ADC_Address__c getCustomerAddress(@PathVariable("customerid") Long customerid) {
		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
            System.out.println("404 enconter");
        }
		return new ADC_Address__c();
	}

	@Override
	public ADC_Address__c getCustomerAddressByAddressId(Long customer_id, Long id) {
		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
            System.out.println("404 enconter");
        }
		return new ADC_Address__c();
	}

	@Override
	public Long createCustomerAddress(ADC_Address__c address__c, Long customer_id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateAddress(Long customer_id, Long address_id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ADC_Permission__c> getCommunicationDetails() {

		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
            System.out.println("404 enconter");
        }
		return new ArrayList<ADC_Permission__c>() ;
	}

	@Override
	public ADC_Permission__c getCommunicationDetailsById(Long customerid) {
		// TODO Auto-generated method stub
		return null;
	}
	
	*/
}

