package com.abbott.adc.adapter.webshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abbott.adc.adapter.service.businessevent.CustomerService;
import com.abbott.adc.adapter.webshop.dto.Customer;

/**
 * This class is used to call the endpoints for retrieving information and  create.
 * @author shkalyan
 *
 */
@RestController
@RequestMapping("/api")
public class CustomerController {

	/*@Autowired
	AccountService accountService;*/
	
	@Autowired
	CustomerService customerService;
	/**
	 * this method will return list of all customer created.
	 */
	@GetMapping("/customers")
	public List<Customer> getAllCustomer() {
		return customerService.getCustomer();
	}
	
	/**
	 * Used to get all accounts details
	 * @return
	 */
	/*@GetMapping("/accounts")
	public List<Account> getAllAccount() {
		return accountService.getAllAccount();
	
	}*/
	
	/**
	 * method will fetch  accounts data from id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	/*@GetMapping("/customers/{id}")
	public Customer getAccountsById(@PathVariable Long id) throws Exception {
		Customer cust = accountService.getAccountsById(id);
		return cust;
	}*/
	
	/*@PostMapping("/accounts/create")
	public void createCustomer(@RequestBody Account acc) {
		Long custid = accountService.createCustomer(acc);
	}
	
	@PutMapping("/accounts/{customerid}")
	public void updateAccount(@RequestBody Customer cust) {
		
	}*/
	
}