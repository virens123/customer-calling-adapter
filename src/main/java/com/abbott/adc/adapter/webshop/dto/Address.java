package com.abbott.adc.adapter.webshop.dto;

import java.util.Date;

public class Address {
	
	private Long entityId;
	private String prefix;
	private String firstName;
	private String lastName;
	private String street;
	private String city;
	private String postCode;
	private Date region;
	private String countryId;
	private String telephone;
	private String fax;
	private String vatNumber;
	private String customerAddressLabel;
	private String pickpointTerminalId;
	private String pickpointTerminalName;
	private String missingVerification;
	private String isDefaultBilling;
	private String isDefaultShipping;
	private String company;
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public Date getRegion() {
		return region;
	}
	public void setRegion(Date region) {
		this.region = region;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getCustomerAddressLabel() {
		return customerAddressLabel;
	}
	public void setCustomerAddressLabel(String customerAddressLabel) {
		this.customerAddressLabel = customerAddressLabel;
	}
	public String getPickpointTerminalId() {
		return pickpointTerminalId;
	}
	public void setPickpointTerminalId(String pickpointTerminalId) {
		this.pickpointTerminalId = pickpointTerminalId;
	}
	public String getPickpointTerminalName() {
		return pickpointTerminalName;
	}
	public void setPickpointTerminalName(String pickpointTerminalName) {
		this.pickpointTerminalName = pickpointTerminalName;
	}
	public String getMissingVerification() {
		return missingVerification;
	}
	public void setMissingVerification(String missingVerification) {
		this.missingVerification = missingVerification;
	}
	public String getIsDefaultBilling() {
		return isDefaultBilling;
	}
	public void setIsDefaultBilling(String isDefaultBilling) {
		this.isDefaultBilling = isDefaultBilling;
	}
	public String getIsDefaultShipping() {
		return isDefaultShipping;
	}
	public void setIsDefaultShipping(String isDefaultShipping) {
		this.isDefaultShipping = isDefaultShipping;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}

	

}
