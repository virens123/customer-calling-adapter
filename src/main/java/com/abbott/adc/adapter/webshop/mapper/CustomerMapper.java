/*package com.abbott.adc.adapter.webshop.mapper;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import com.abbott.adc.adapter.webshop.dto.Customer;
import com.sforce.soap.enterprise.sobject.Account;

@Component
public class CustomerMapper {

	public Customer csrToMagentoMapper(Account account) throws Exception {
		
		Customer cust= new Customer();
		cust.setEntityId(101L);
		cust.setCountryCode(account.getADC_Account_Country_Code__pc());
		cust.setLanguageCode(account.getADC_Language_Code__pc());
		cust.setPrefix(account.getPersonTitle());
		cust.setFirstName(account.getFirstName());
		cust.setLastName(account.getLastName());
		cust.setEmail(account.getPersonEmail());
		cust.setEmailVerified(Boolean.valueOf(account.getADC_Email_Verified__c()));
		//cust.setActive(account.getADC_Active__c());
		
        //TODO need to create and add utility
        //cust.setDob(account.getPersonBirthdate().getTime());
        cust.setCustomerLandline(account.getPhone());
        cust.setCustomerMobile(account.getPersonMobilePhone());
      //  cust.setCreatedAt(Timestamp.valueOf(account.getADC_Created_Date_Text__c()));
       // cust.setUpdatedAt(Timestamp.valueOf(account.getLast_Modified_Date_Text__c()));
        cust.setUpdatedSource(account.getADC_Updated_source__c());
        cust.setFiscalCode(account.getADC_Fiscal_Code__c());
        cust.setInvitedCustomerStatus(account.getADC_DTP_Invited_Customer_Status__pc());
        //TODO need to create and add utility
       // cust.setInvitedCustomerStatusDate(new Timestamp(account.getADC_DTP_Invited_Cust_Status_Chg_DateTime__pc().getTimeInMillis()));
       // cust.setPayerInstitution(Integer.valueOf(account.getADC_Payer_Institution_ID__c()));
       // cust.setGhostAccountId(Integer.valueOf(account.getADC_Partner_Ghost_Customer_ID__c()));
        cust.setInsuranceNumber(account.getADC_Insurance_Number__c());
        //cust.setOffLine(account.getADC_Is_off_line__c());
       // cust.setTermsAndConditionAgreed(account.getADC_Terms_and_conditions_agrred__c());
       // cust.setSignatureCaputred(account.getADC_Signature_Captured__c());
        //cust.setDataPrivacy(account.getADC_Data_Privacy_Agreed__c());
        cust.setCreatedFrom(account.getADC_Created_from__c());
       // cust.setUnder18(account.getADC_Under_18__c());
        //cust.setExemptedFromPayingCopayment(account.getADC_Exempted_from_paying_copayment__c());
        //cust.setPrepaymentChoice(account.getADC_Prepayment_Choice__c().intValue());
        cust.setPaymentMethod(account.getPayment_Method__c());
        cust.setMeasurement(account.getADC_Measurement__c());
        cust.setSamplingProgramCode(account.getADC_Sampling_program_code__c());
       // cust.setSamplingOrderPlaced(account.getADC_Sampling_Order_Placed__c());
       // cust.setSubscribedForVatReduction(account.getADC_Is_subscribed_for_VAT_reduction__c());
        //TODO need to create and add utility
       // cust.setVatReductionStartDate(account.getADC_VAT_Reduction_Start_date__c().getTime());
        //TODO need to create and add utility
       // cust.setVatReductionEndDate(account.getADC_VAT_Reduction_End_date__c().getTime());
        cust.setDniInf(account.getADC_DNI_INF__c());
        cust.setTaxVatNumber(account.getADC_Tax_VAT_Number__c());
        cust.setNif(account.getADC_NIF__c());
        cust.setNip(account.getADC_NIP__c());
     //   cust.setPrivacyConsent(account.getADC_Privacy_Conscent__c());
    //    cust.setPrivacySurvey(account.getADC_Privacy_Survey__c());
        
		return cust;
		
	}
}
*/