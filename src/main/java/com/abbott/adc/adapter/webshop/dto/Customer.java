package com.abbott.adc.adapter.webshop.dto;

import java.sql.Timestamp;
import java.util.Date;

public class Customer {

	private Long entityId;
	private String countryCode;
	private String languageCode;
	private String prefix;
	private String firstName;
	private String lastName;
	private String email;
	private boolean emailVerified;
	private boolean isActive;
	private String groupId;
	private Date dob;

	private String customerLandline;
	
	private String customerMobile;
	
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private String updatedSource;
	
	private String fiscalCode;
	
	private String invitedCustomerStatus;
	private  Timestamp invitedCustomerStatusDate;
	private Integer payerInstitution;
	private Integer ghostAccountId;
	private String insuranceNumber;
	private boolean isOffLine;
	private boolean termsAndConditionAgreed;
	private boolean signatureCaputred;
	private boolean dataPrivacy;
	private String createdFrom;
	private boolean under18;
	private boolean exemptedFromPayingCopayment;
	private Integer prepaymentChoice;
	private String paymentMethod;
	private String measurement;
	private String samplingProgramCode;
	private boolean samplingOrderPlaced;
	private boolean isSubscribedForVatReduction;
	private Date vatReductionStartDate;
	private Date vatReductionEndDate;
	private String dniInf;
	private String taxVatNumber;
	private String nif;
	private String nip;
    private boolean privacyConsent;
    private boolean privacySurvey;
    
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isEmailVerified() {
		return emailVerified;
	}
	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getCustomerLandline() {
		return customerLandline;
	}
	public void setCustomerLandline(String customerLandline) {
		this.customerLandline = customerLandline;
	}
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getUpdatedSource() {
		return updatedSource;
	}
	public void setUpdatedSource(String updatedSource) {
		this.updatedSource = updatedSource;
	}
	public String getFiscalCode() {
		return fiscalCode;
	}
	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}
	public String getInvitedCustomerStatus() {
		return invitedCustomerStatus;
	}
	public void setInvitedCustomerStatus(String invitedCustomerStatus) {
		this.invitedCustomerStatus = invitedCustomerStatus;
	}
	public Timestamp getInvitedCustomerStatusDate() {
		return invitedCustomerStatusDate;
	}
	public void setInvitedCustomerStatusDate(Timestamp invitedCustomerStatusDate) {
		this.invitedCustomerStatusDate = invitedCustomerStatusDate;
	}
	public Integer getPayerInstitution() {
		return payerInstitution;
	}
	public void setPayerInstitution(Integer payerInstitution) {
		this.payerInstitution = payerInstitution;
	}
	public Integer getGhostAccountId() {
		return ghostAccountId;
	}
	public void setGhostAccountId(Integer ghostAccountId) {
		this.ghostAccountId = ghostAccountId;
	}
	public String getInsuranceNumber() {
		return insuranceNumber;
	}
	public void setInsuranceNumber(String insuranceNumber) {
		this.insuranceNumber = insuranceNumber;
	}
	public boolean isOffLine() {
		return isOffLine;
	}
	public void setOffLine(boolean isOffLine) {
		this.isOffLine = isOffLine;
	}
	public boolean isTermsAndConditionAgreed() {
		return termsAndConditionAgreed;
	}
	public void setTermsAndConditionAgreed(boolean termsAndConditionAgreed) {
		this.termsAndConditionAgreed = termsAndConditionAgreed;
	}
	public boolean isSignatureCaputred() {
		return signatureCaputred;
	}
	public void setSignatureCaputred(boolean signatureCaputred) {
		this.signatureCaputred = signatureCaputred;
	}
	public boolean isDataPrivacy() {
		return dataPrivacy;
	}
	public void setDataPrivacy(boolean dataPrivacy) {
		this.dataPrivacy = dataPrivacy;
	}
	public String getCreatedFrom() {
		return createdFrom;
	}
	public void setCreatedFrom(String createdFrom) {
		this.createdFrom = createdFrom;
	}
	public boolean isUnder18() {
		return under18;
	}
	public void setUnder18(boolean under18) {
		this.under18 = under18;
	}
	public boolean isExemptedFromPayingCopayment() {
		return exemptedFromPayingCopayment;
	}
	public void setExemptedFromPayingCopayment(boolean exemptedFromPayingCopayment) {
		this.exemptedFromPayingCopayment = exemptedFromPayingCopayment;
	}
	public Integer getPrepaymentChoice() {
		return prepaymentChoice;
	}
	public void setPrepaymentChoice(Integer prepaymentChoice) {
		this.prepaymentChoice = prepaymentChoice;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getMeasurement() {
		return measurement;
	}
	public void setMeasurement(String measurement) {
		this.measurement = measurement;
	}
	public String getSamplingProgramCode() {
		return samplingProgramCode;
	}
	public void setSamplingProgramCode(String samplingProgramCode) {
		this.samplingProgramCode = samplingProgramCode;
	}
	public boolean isSamplingOrderPlaced() {
		return samplingOrderPlaced;
	}
	public void setSamplingOrderPlaced(boolean samplingOrderPlaced) {
		this.samplingOrderPlaced = samplingOrderPlaced;
	}
	public boolean isSubscribedForVatReduction() {
		return isSubscribedForVatReduction;
	}
	public void setSubscribedForVatReduction(boolean isSubscribedForVatReduction) {
		this.isSubscribedForVatReduction = isSubscribedForVatReduction;
	}
	public Date getVatReductionStartDate() {
		return vatReductionStartDate;
	}
	public void setVatReductionStartDate(Date vatReductionStartDate) {
		this.vatReductionStartDate = vatReductionStartDate;
	}
	public Date getVatReductionEndDate() {
		return vatReductionEndDate;
	}
	public void setVatReductionEndDate(Date vatReductionEndDate) {
		this.vatReductionEndDate = vatReductionEndDate;
	}
	public String getDniInf() {
		return dniInf;
	}
	public void setDniInf(String dniInf) {
		this.dniInf = dniInf;
	}
	public String getTaxVatNumber() {
		return taxVatNumber;
	}
	public void setTaxVatNumber(String taxVatNumber) {
		this.taxVatNumber = taxVatNumber;
	}
	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public boolean isPrivacyConsent() {
		return privacyConsent;
	}
	public void setPrivacyConsent(boolean privacyConsent) {
		this.privacyConsent = privacyConsent;
	}
	public boolean isPrivacySurvey() {
		return privacySurvey;
	}
	public void setPrivacySurvey(boolean privacySurvey) {
		this.privacySurvey = privacySurvey;
	}
	
	
	


	
	
}
