/*package com.abbott.adc.adapter.webshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abbott.adc.adapter.service.businessevent.AccountService;
import com.abbott.adc.adapter.webshop.dto.Communication;
import com.sforce.soap.enterprise.sobject.ADC_Permission__c;

*//**
 * This class is to retrive update and create the information regarding permission
 * @author shkalyan
 *
 *//*
@RestController
@RequestMapping("/api")
public class PermissionController {

	@Autowired
	AccountService accountService;
	
	*//**
	 * This method is used to get CommunicationDetails
	 * @return
	 *//*
	@GetMapping("/Permission")
	public List<ADC_Permission__c> getCommunicationDetails() {
		return accountService.getCommunicationDetails();
	}
	
	*//**
	 * This method will fetch  accounts data from id
	 * @param id
	 * @return
	 * @throws Exception
	 *//*
	@GetMapping("/Permission/{id}")
	public Communication getCommunicationDetailsById(@PathVariable Long id) throws Exception {
		Communication communication = accountService.getCommunicationDetailsById(id);
		return communication;
	}
}
*/