/*package com.abbott.adc.adapter.webshop.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.abbott.adc.adapter.service.businessevent.AccountService;
import com.abbott.adc.adapter.webshop.dto.Address;

*//**
 * This class is used to retrive and create and update information of address.
 * @author shkalyan
 *
 *//*
@RestController
@RequestMapping("/api")
public class AddressController {

	@Autowired
	AccountService accountService;
	
	*//**
	 * This method is used to get customer address from customer id
	 * @param customer_id
	 * @return
	 * @throws Exception
	 *//*
	@GetMapping("/customer/{customer_id}/listaddress")
	public Address getCustomerAddress(@PathVariable Long customer_id) throws Exception {
		Address address = accountService.getCustomerAddress(customer_id);
		return address;
	}
	
	*//**
	 * Used to retrieve customer address from customer id and address id
	 * @param customer_id
	 * @param id
	 * @return
	 * @throws Exception
	 *//*
	@GetMapping("/customer/{customer_id}/address/{id}/retrieve")
	public Address getCustomerAddressByAddressId(@PathVariable("customer_id") Long customer_id,@PathVariable("id") Long id) throws Exception {
		Address address = accountService.getCustomerAddressByAddressId(customer_id,id);
		return address;
	}
}
*/